console.log("1.")
console.log(countries)
console.log(webTechs)

let text ='I love teaching and empowering people. I teach HTML, CSS, JS, React, Python.'

console.log("2.",text.split(/[\s,]+/).map(data => {
    return data.replace(".", "")
}))


let shoppingCart = ['Milk', 'Coffee', 'Tea', 'Honey']
shoppingCart = [ "Meat", ...shoppingCart]
console.log("3.1",shoppingCart)
shoppingCart.push("Sugar")
console.log("3.2",shoppingCart)
console.log("3.3",shoppingCart.filter(data => {
    return data != "Honey"
}))
shoppingCart[3] = "Green tea"
console.log("3.4",shoppingCart)

let countryText = "Tao Go"
let checkCountry = function(country) {
    var alert = ""
    if (countries.includes(country)) {
        alert = country.toUpperCase()
    }else {
        countries.push(country)
        alert = `${country} Has been saved`
    }
    return alert
}
console.log("4.", checkCountry(countryText))

let tech = "Sass"
let checkTech = function(tech) {
    var alert = ""
    if (webTechs.includes(tech)) {
        alert = `${tech.toUpperCase()} is a CSS preprocess`
    }else {
        webTechs.push(tech)
        alert = `${tech} Has been saved`
    }
    return alert
}

console.log("5.", checkTech(tech))

const frontEnd = ['HTML', 'CSS', 'JS', 'React', 'Redux']
const backEnd = ['Node','Express', 'MongoDB']
let fullStack = frontEnd.concat(backEnd)
console.log("6.",fullStack)
