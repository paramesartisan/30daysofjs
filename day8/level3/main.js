const users_u = [
  {
    _id: 'ab12ex',
    username: 'Alex',
    email: 'alex@alex.com',
    password: '123123',
    createdAt: '08/01/2020 9:00 AM',
    isLoggedIn: false,
  },
  {
    _id: 'fg12cy',
    username: 'Asab',
    email: 'asab@asab.com',
    password: '123456',
    createdAt: '08/01/2020 9:30 AM',
    isLoggedIn: true,
  },
  {
    _id: 'zwf8md',
    username: 'Brook',
    email: 'brook@brook.com',
    password: '123111',
    createdAt: '08/01/2020 9:45 AM',
    isLoggedIn: true,
  },
  {
    _id: 'eefamr',
    username: 'Martha',
    email: 'martha@martha.com',
    password: '123222',
    createdAt: '08/01/2020 9:50 AM',
    isLoggedIn: false,
  },
  {
    _id: 'ghderc',
    username: 'Thomas',
    email: 'thomas@thomas.com',
    password: '123333',
    createdAt: '08/01/2020 10:00 AM',
    isLoggedIn: false,
  },
]

const products = [
  {
    _id: 'eedfcf',
    name: 'mobile phone',
    description: 'Huawei Honor',
    price: 200,
    ratings: [
      { userId: 'fg12cy', rate: 5 },
      { userId: 'zwf8md', rate: 4.5 },
    ],
    likes: [],
  },
  {
    _id: 'aegfal',
    name: 'Laptop',
    description: 'MacPro: System Darwin',
    price: 2500,
    ratings: [],
    likes: ['fg12cy'],
  },
  {
    _id: 'hedfcg',
    name: 'TV',
    description: 'Smart TV:Procaster',
    price: 400,
    ratings: [{ userId: 'fg12cy', rate: 5 }],
    likes: ['fg12cy'],
  },
]
const entriess_u = Object.entries(users_u)
const sign_up = {
  _id: 'ab12ex',
  username: 'Alex',
  email: 'alexTao@alex.com',
  password: '123123',
  createdAt: '08/01/2020 9:00 AM',
  isLoggedIn: false,
}
entriess_u.forEach((e) => {
  if (sign_up.email === e[1].email) {
    console.log('Dupicate')
  } else {
    users_u.push(sign_up)
  }
})

function rateProduct(id, user_id, rate) {
  const rate_p = Object.entries(products)
  rate_p.forEach((e) => {
    if (e[1]._id === id) {
      e[1].ratings.push({
        user_id: user_id,
        ratings: rate,
      })
    }
  })
}

function averageRating() {
  const rate_a = Object.entries(products)
  rate_a.forEach((e) => {
    if (null != e[1].ratings[0]) {
      console.log(e[1].ratings[0].rate)
    }
  })
}
averageRating()

function likeProduct() {
  const rate_a = Object.entries(products)
  rate_a.forEach((e) => {
    if (null == e[1].likes[0]) {
      console.log(e[1].pop)
    }
  })
}
likeProduct()
console.log(products)
